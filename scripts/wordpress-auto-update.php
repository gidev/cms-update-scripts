<?php
/**
 * Created by PhpStorm.
 * User: Christian Giegler
 * Date: 13.02.2020
 * Time: 13:55
 */

/**
 * Create database dump
 */

header("Transfer-encoding: chunked");
@apache_setenv('no-gzip', 1);
@ini_set('zlib.output_compression', 0);
@ini_set('implicit_flush', 1);

for ($i = 0; $i < ob_get_level(); $i++)  ob_end_flush();
ob_implicit_flush(1); flush();

function dump_chunk($chunk)
{
    printf("%x\r\n%s\r\n", strlen($chunk), $chunk);
    flush();
}


if (file_exists('wp-config.php')) {
    include('wp-config.php');
} else {
    echo "Couldn't find wp-config.php";
}
$user = DB_USER;
$pass = DB_PASSWORD;
$host = DB_HOST;
$db_name = DB_NAME;

exec("mysqldump --quick -Q -a --allow-keywords -f --add-drop-table --extended-insert --routines --user={$user} --password={$pass} --host={$host} --databases {$db_name} 2> dump-error.log | /bin/gzip > {$db_name}.sql.gz");

dump_chunk("Database dump created"."<br>");
flush();

/**
 * Create wordpress backup
 */

exec("zip -r ./update_backup.zip .htaccess liesmich.html readme.html index.php wp-activate.php wp-blog-header.php wp-comments-post.php wp-config.php wp-cron.php wp-links-opml.php wp-load.php wp-login.php wp-mail.php wp-settings.php wp-signup.php wp-trackback.php xmlrpc.php license.txt db_backup.sql  wp-admin wp-content wp-includes");

dump_chunk("Backup of the current wordpress installation including database dump created"."<br>");
flush();

/**
 * Download and extract wordpress
 */

$ch = curl_init();
$source = "https://de.wordpress.org/latest-de_DE.zip"; //$source = $dynamic_url
curl_setopt($ch, CURLOPT_URL, $source);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
$data = curl_exec($ch);
curl_close($ch);
if (!file_exists('tmp_2020wp')) {
    mkdir('tmp_2020wp', 0777, true);
}
$destination = "tmp_2020wp/wordpress.zip";
$file = fopen($destination, "w+");
fputs($file, $data);
fclose($file);

$file = 'tmp_2020wp/wordpress.zip';

$path = pathinfo(realpath($file), PATHINFO_DIRNAME);

$zip = new ZipArchive;
$res = $zip->open($file);
if ($res === TRUE) {
    $zip->extractTo($path);
    $zip->close();

    /**
     * Remove old files
     */

    exec("rm -f ./liesmich.html");
    exec("rm -f ./readme.html");
    exec("rm -f ./index.php");
    exec("rm -f ./wp-activate.php");
    exec("rm -f ./wp-blog-header.php");
    exec("rm -f ./wp-comments-post.php");
    exec("rm -f ./wp-cron.php");
    exec("rm -f ./wp-links-opml.php");
    exec("rm -f ./wp-load.php");
    exec("rm -f ./wp-login.php");
    exec("rm -f ./wp-mail.php");
    exec("rm -f ./wp-settings.php");
    exec("rm -f ./wp-signup.php");
    exec("rm -f ./wp-trackback.php");
    exec("rm -f ./xmlrpc.php");
    exec("rm -f ./license.txt");
    exec("rm -rf ./wp-admin");
    exec("rm -rf ./wp-includes");

    dump_chunk("Old files were deleted"."<br>");
    flush();

    /**
     * Copy new files
     */

    exec("cp -rf ./tmp_2020wp/wordpress/. ./");

    dump_chunk("New files successfully completed"."<br>");
    flush();

    /**
     * Remove tmp files
     */

    exec("rm -rf ./tmp_2020wp");

    dump_chunk("The temporary folder was deleted. <p style='color: red'> The update was successfully completed, please open the wordpress backend to complete the database update </p>"."<br>");
    flush();
} else {
    dump_chunk("There was an error while extracting the zip"."<br>");
    flush();
}